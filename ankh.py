#!/usr/bin/python

# Copyright 2012 Anton Mihalev. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL ANTON MIHALEV OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are
# those of the authors and should not be interpreted as representing official
# policies, either expressed or implied, of Anton Mihalev.


import binascii
import bz2
import datetime
import errno
import hashlib
import locale
import os
import stat
import sys
import optparse
import unicodedata

class FileState(object):
    GOOD    = 0
    DAMAGED = 1
    MISSING = 2
    UNKNOWN = 3

    __text = (
        '\033[92mGOOD\033[0m',
        '\033[91mDAMG\033[0m',
        '\033[91mMISS\033[0m',
        '\033[91mUNKN\033[0m',
    )

    @staticmethod
    def format(state):
        return FileState.__text[state]

class ApplicationError(Exception):
    pass

class ParseError(Exception):
    pass

class TreeDigestException(Exception):
    def __init__(self, dirname, strerror):
        self.dirname = dirname
        self.strerror = strerror

    def __str__(self):
        return self.strerror

class FileDigest(object):
    def __init__(self, path, basename):
        self.__path = path
        self.__basename = basename
        self.__digest = None
        self.__mtime = None
        self.__size = None

    def __stat(self):
        st = os.stat(self.__path + '/' + self.__basename)
        self.__mtime = st[stat.ST_MTIME]
        self.__size = st[stat.ST_SIZE]

    def basename(self):
        """
        Returns base name of the file.
        """
        return self.__basename

    def filename(self):
        """
        Returns filename of the file (path to the file + name of the file).
        """
        return self.__path + '/' + self.__basename

    def digest(self):
        """
        Returns checksum of the file.
        """
        # Calculate checksum if don't have it yet.
        if self.__digest is None:
            md5 = hashlib.md5()
            src = open(self.filename())
            while True:
                chunk = src.read(1048576)
                if len(chunk) > 0:
                    md5.update(chunk)
                else:
                    break
            src.close()
            self.__digest = md5.digest()

        return self.__digest

    def mtime(self):
        """
        Returns time of last modification (UNIX time).
        """
        if self.__mtime is None:
            self.__stat()
        return self.__mtime

    def size(self):
        """
        Returns size of the file in bytes.
        """
        if self.__size is None:
            self.__stat()
        return self.__size

    def serialize(self, stream):
        """
        Serializes `FileDigest` object into `stream`.
        """
        result = ''.join([binascii.hexlify(self.digest()), ' ',
                          str(self.mtime()), ' ', str(self.size()), ' ',
                          self.__basename.encode('utf-8'), '\n'])
        stream.write(result)

    @staticmethod
    def deserialize(stream, path):
        """
        Tries to restore `FileDigest` from the `stream`.
        `path` must be a path to a directory that contains this file.
        """
        line = stream.readline()[:-1]
        if len(line) == 0:
            return None

        try:
            (digest, mtime, size, filename) = line.split(' ', 3)
        except Exception as e:
            raise ParseError('FileDigest quadruple is corrupted (%s)' % e)
        try:
            filename = filename.decode('utf-8')
        except Exception as e:
            raise ParseError('FileDigest string contains invalid F-NAME (%s)'
                               % e)

        checksum = FileDigest(path, filename)
        try:
            checksum.__digest = binascii.unhexlify(digest)
        except Exception as e:
            raise ParseError('FileDigest string contains invalid DIGEST (%s)'
                               % e)
        try:
            checksum.__mtime = int(mtime)
        except Exception as e:
            raise ParseError('FileDigest string contains Invalid M-TIME (%s)'
                               % e)
        try:
            checksum.__size = int(size)
        except Exception as e:
            raise ParseError('FileDigest string contains invalid F-SIZE (%s)'
                               % e)
        return checksum

class TreeDigest(object):
    def __init__(self, path):
        self.__path = path
        self.__snapshots = None
        self.__file_count = None

    @staticmethod
    def __should_ignore_file(basename):
        # TODO: Implement proper filter.
        if basename == '.DS_Store':
            return True
        else:
            return False

    def __add_directory_recursive(self, dir_path):
        def throw(err):
            raise err

        new_file_count = 0
        for (path, _, files) in os.walk(dir_path, onerror=throw):
            # Normalize Unicode string. Normalization is required to
            # make strings returned by MacOS X Filesystem API and
            # Linux Filesystem API comparable.
            path = unicodedata.normalize('NFC', path)

            snapshots = []
            for filename in files:
                # Normalize file basename as well.
                filename = unicodedata.normalize('NFC', filename)

                if self.__should_ignore_file(filename):
                    continue
                snapshots.append(FileDigest(path, filename))

            self.__snapshots[path] = snapshots
            new_file_count += len(snapshots)

        self.__file_count = self.file_count() + new_file_count

    def __find_changes_recursive(self, path, changes_accumulator):
        live_list = os.listdir(path)
        live_dirs = set()
        live_files = set()
        snapshot_files = set()

        path = unicodedata.normalize('NFC', path)
        live_list = [unicodedata.normalize('NFC', i) for i in os.listdir(path)]

        # Separate files and directories into two sets.
        for basename in live_list:
            if self.__should_ignore_file(basename):
                continue

            st = os.stat(path + '/' + basename)
            if stat.S_ISDIR(st.st_mode):
                live_dirs.add(basename)
            else:
                live_files.add(basename)

        # Populate snapshot_files set with names of files from
        # snapshot. Populate to_compare list with the files
        # common to both live_files set and snapshot_files set.
        common_files = []
        for fsnap in self.__snapshots[path]:
            basename = fsnap.basename()
            snapshot_files.add(basename)
            if basename in live_files:
                common_files.append(fsnap)

        # Check common files for changes.
        changed_files = {}
        for fsnap in common_files:
            live_fsnap = FileDigest(path, fsnap.basename())

            # Skip the file if size and modification time are same
            # between live and repository's versions.
            if fsnap.mtime() == live_fsnap.mtime() and \
               fsnap.size() == live_fsnap.size():
                continue
            elif fsnap.size() != live_fsnap.size() or \
                 fsnap.digest() != live_fsnap.digest():
                changed_files[fsnap.basename()] = (live_fsnap, fsnap)

        # Find newly created directories.
        new_dirs = []
        for basename in live_dirs:
            filename = path + '/' + basename
            if filename not in self.__snapshots:
                new_dirs.append(basename)
            else:
                self.__find_changes_recursive(filename, changes_accumulator)

        # Find removed directories.
        removed_dirs = []
        for filename in self.__snapshots.iterkeys():
            if os.path.dirname(filename) == path:
                basename = os.path.basename(filename)
                if basename not in live_dirs:
                    removed_dirs.append(basename)

        new_files = live_files - snapshot_files
        removed_files = snapshot_files - live_files
        if len(new_files) != 0 or len(removed_files) != 0 or \
           len(changed_files) != 0 or len(new_dirs) != 0 or \
           len(removed_dirs) != 0:
            changes_accumulator[path] = {
                'new_files': live_files - snapshot_files,
                'removed_files': snapshot_files - live_files,
                'changed_files': changed_files,
                'new_dirs': new_dirs,
                'removed_dirs': removed_dirs,
            }

    def file_count(self):
        if self.__file_count is None:
            self.__file_count = reduce(lambda acc, x: acc + len(x),
                                       self.__snapshots.itervalues(), 0)
        return self.__file_count

    def populate(self):
        self.__snapshots = {}
        self.__file_count = 0
        self.__add_directory_recursive(self.__path)

    def regenerate(self):
        self.populate()

        fileno = 1
        file_count = self.file_count()
        for (path, file_digests) in self.__snapshots.iteritems():
            for digest in file_digests:
                yield (digest.filename(), float(fileno) / file_count * 100)

                # Force digest to compute by accessing its value.
                digest.digest()
                fileno += 1

    def apply_changes(self, all_changes):
        for (path, changes) in all_changes.iteritems():
            # Actualize directory list.
            for dirname in changes['removed_dirs']:
                del self.__snapshots[path + '/' + dirname]
            for dirname in changes['new_dirs']:
                self.__add_directory_recursive(path + '/' + dirname)

            # Weed out digests of removed files from the digest list
            # and replace digests of changed files with their updated
            # versions.
            files = []
            changed_files = changes['changed_files']
            removed_files = changes['removed_files']
            for snapshot in self.__snapshots[path]:
                filename = snapshot.basename()
                if filename in removed_files:
                    continue
                elif filename in changed_files:
                    files.append(changed_files[filename][0])
                else:
                    files.append(snapshot)

            # Add digests of new files to the digest list.
            for filename in changes['new_files']:
                files.append(FileDigest(path, filename))

            self.__snapshots[path] = files

    def diff(self):
        changes = {}
        self.__find_changes_recursive(self.__path, changes)
        return changes

    def verify(self):
        """
        A generator function that compares file checksums stored in the
        snapshot and checksums of the live files.

        This function will yield a tuple that contains following items:
        `FileDigest` instance representing current version of the file,
        `FileDigest` instance representing version of the file that
        saved in the repository, and file state.

        File state can take one of the following values:
        `FileState.GOOD`, `FileState.DAMAGED`, `FileState.MISSING`.

        Note that the second field in the tuple is not present when
        file state is not FileState.GOOD or FileState.DAMAGED.
        """
        for (path, saved_file_snapshots) in self.__snapshots.items():
            for saved_fsnap in saved_file_snapshots:
                try:
                    live_fsnap = FileDigest(path, saved_fsnap.basename())
                    saved_digest = saved_fsnap.digest()
                    live_digest = live_fsnap.digest()
                    if saved_digest == live_digest:
                        yield (live_fsnap, saved_fsnap, FileState.GOOD)
                    else:
                        yield (live_fsnap, saved_fsnap, FileState.DAMAGED)
                except IOError as e:
                    if e.errno == errno.ENOENT:
                        yield (None, saved_fsnap, FileState.MISSING)
                    else:
                        # TODO: Redesign things so we can include more
                        # information about error.
                        yield (None, saved_fsnap, FileState.UNKNOWN)

    def serialize(self, stream):
        """
        Serializes `TreeDigest`'s state into `stream`.
        """
        stream.write('%s\n' % (self.__path.encode('utf-8')))
        for (dirname, files) in self.__snapshots.items():
            stream.write('%s\n' % (dirname.encode('utf-8')))
            for fsnap in files:
                stream.write('-')
                fsnap.serialize(stream)
            stream.write('\n')
        stream.write('\n')

    @staticmethod
    def deserialize(stream):
        """
        Deserializes `TreeDigest`'s state from `stream`.
        Stream should be positioned at the start of `TreeDigest`
        header.
        """
        tree_path = None
        try:
            tree_path = stream.readline()[:-1].decode('utf-8')
        except Exception as e:
            msg = 'Invalid TREE-PATH (%s)' % e
            raise ParseError(msg)

        if len(tree_path) == 0:
            raise ParseError('TREE-PATH cannot be empty')

        snapshots = {}
        while True:
            # Extract string containing path to directory that
            # encloses current group of files.
            path = None
            try:
                path = stream.readline()[:-1].decode('utf-8')
            except Exception as e:
                raise ParseError('Invalid DIRECTORY-PATH (%s)' % e)

            # Empty DIRECTORY-PATH means that there are no more
            # directory entries in the current snapshot and that we
            # have successfully parsed input.
            if len(path) == 0:
                break

            # Extract FileDigest entries.
            files = []
            while True:
                magic = stream.read(1)
                if magic == '-':
                    files.append(FileDigest.deserialize(stream, path))
                elif magic == '\n':
                    break
                else:
                    raise ParseError('Corrupted file entry detected')

            snapshots[path] = files

        dsnap = TreeDigest(tree_path)
        dsnap.__snapshots = snapshots
        return dsnap

def open_repository(path):
    # Change working directory to the directory where repository
    # is located.
    working_dir = os.path.dirname(path)
    if len(working_dir) > 0:
        try:
            os.chdir(working_dir)
        except OSError as e:
            raise ApplicationError('Could not change working directory (%s).'
                                     % (working_dir, e.strerror))

    try:
        stream = bz2.BZ2File(path, mode='r')
        return stream
    except IOError as e:
        raise ApplicationError('Could not open repository (%s).'
                                 % (e.strerror))

def read_repository(stream):
    """
    Generator function that reads and restores `TreeDigests`
    instances from the `stream`.
    """
    while True:
        # We store a magic value at the beginning of each snapshot.
        # Missing magic indicates the end of stream.
        try:
            magic = stream.read(1)
            if magic == '$':
                yield TreeDigest.deserialize(stream)
                continue
            else:
                # TODO: Terminate only on empty lines.
                break
        except ParseError as e:
            raise ApplicationError(
                'Repository has its metadata corrupted: %s' % e)
        except IOError as e:
            raise ApplicationError('Could not read repository (%s)'
                                     % str(e).capitalize())

def write_repository(filename, snapshots):
    # Open repository file for writing.
    stream = None
    try:
        stream = bz2.BZ2File(filename, mode='w')
    except OSError as e:
        raise ApplicationError(
            'Could not open repository file for writing (%s)' % (e))

    # Create TreeDigest instance for every user-specified
    # directory and serialize it.
    try:
        for snapshot in snapshots:
            stream.write('$')
            snapshot.serialize(stream)
    except IOError as e:
        raise ApplicationError('Unable to write repository file (%s)' % e)
    finally:
        stream.close()

def print_changes(all_changes):
    for (path, changes) in all_changes.iteritems():
        if len(changes['new_dirs']) > 0 or len(changes['removed_dirs']) > 0:
            print('\033[95m%s\033[0m (Folders):' % (path))
            for basename in changes['removed_dirs']:
                print('[\033[31mD\033[0m] %s' % (basename))
            for basename in changes['new_dirs']:
                print('[\033[32mN\033[0m] %s' % (basename))
            print('')

        if len(changes['new_files']) > 0 or \
           len(changes['removed_files']) > 0 or \
           len(changes['changed_files']) > 0:
            print('\033[95m%s\033[0m (Files):' % (path))
            for filename in changes['changed_files'].iterkeys():
                print('[\033[33mC\033[0m] %s' % (filename))
            for filename in changes['removed_files']:
                print('[\033[31mD\033[0m] %s' % (filename))
            for filename in changes['new_files']:
                print('[\033[32mN\033[0m] %s' % (filename))
            print('')

def print_damaged_file_info(live, saved):
    """
    Prints detailed information about damaged file.

    `live` is an instance of `FileDigest` that was created from live
    filesystem.
    `saved` is an instance of `FileDigest` that was loaded from the
    repository.
    """
    left_csum = binascii.hexlify(saved.digest())
    right_csum = binascii.hexlify(live.digest())

    left_size = locale.format('%d', saved.size(), True)
    right_size = locale.format('%d', live.size(), True)
    size_color = '\033[92m' if saved.size() == live.size() else '\033[91m'

    left_date = datetime.datetime.fromtimestamp(saved.mtime())
    right_date = datetime.datetime.fromtimestamp(live.mtime())
    date_color = '\033[92m' if left_date == right_date else '\033[91m'

    print('[%s] %s\n'
          'Checksum : \033[92m%s\033[0m -> \033[91m%s\033[0m\n'
          'Size     : \033[92m%s\033[0m -> %s%s\033[0m (bytes)\n'
          'Date     : \033[92m%s\033[0m -> %s%s\033[0m\n'
            % (FileState.format(FileState.DAMAGED), saved.filename(),
               left_csum, right_csum,
               left_size, size_color, right_size,
               left_date, date_color, right_date))

def print_bad_file_info(file_digest, file_state):
    """
    Prints information about damaged file.

    This function prints just a brief information about the file.
    For files with `FileState.DAMAGED` status `print_damaged_file_info`
    should be used instead.

    `file_digest` is an instance of `FileDigest` that was loaded from
    the repository.
    `file_state` is a value from `FileState` enum.
    """
    print('[%s] %s' % (FileState.format(file_state), file_digest.filename()))

def verify_tree(w, damage_accumulator, quiet=False):
    """
    Verifies given `TreeDigest`.

    Damaged files are accumulated in the `damage_accumulator` list.
    Each list entry is a tuple yielded by the `TreeDigest.verify()`
    method.

    Set keyword-parameter `quiet` to True to suppress all non-error
    output.
    """
    for (live_fsnap, saved_fsnap, file_state) in w.verify():
        if file_state == FileState.GOOD:
            if not quiet:
                print('[%s] %s' % (FileState.format(file_state),
                                   saved_fsnap.filename()))
        else:
            damaged_file = (live_fsnap, saved_fsnap, file_state)
            damage_accumulator.append(damaged_file)
            print('[%s] %s' % (FileState.format(file_state),
                               saved_fsnap.filename()))

def invoke_new(args):
    """
    An entry point for the 'new' command.
    Creates a new repository.

    The `args` parameter contains all user-specified command-line
    arguments for this action.
    """
    op = optparse.OptionParser(
            usage='%prog new [options] repository-name snapshotes...',
            description='Create a new repository.')
    op.add_option('-q', '--quiet', dest='quiet', action='store_true',
                  help='be less verbose')
    (options, args) = op.parse_args(args)

    if len(args) == 0:
        raise ApplicationError('Repository name is not specified.')
    elif len(args) == 1:
        raise ApplicationError('Creating empty repository is not allowed.\n'
                               'You need to specify one or more directories '
                               'to add to repository.')

    # Create TreeDigest instance for every user-specified
    # directory and serialize it.
    snapshots = []
    try:
        for path in args[1:]:
            snapshot = TreeDigest(path.decode('utf-8'))
            for (filename, progress) in snapshot.regenerate():
                if not options.quiet:
                    print('[\033[92m%3d%%\033[0m] %s' % (progress, filename))
            snapshots.append(snapshot)
    except OSError as e:
        if e.filename is not None:
            raise ApplicationError('%s: \'%s\'' % (e.strerror, e.filename))
        else:
            raise ApplicationError('Unable to generate checksums (%s)'
                                     % e.strerror)
    except IOError as e:
        raise ApplicationError('Unable to generate checksums (%s)' % e)

    write_repository(args[0], snapshots)

    if not options.quiet:
        print('')
    print('Repository was created successfully.')
    return 0

def invoke_apply(args):
    """
    An entry point for the 'apply' command.
    Applies changes to the repository.
    """
    op = optparse.OptionParser(
            usage='%prog apply [options] repository-name',
            description='Apply changes to the repository.')
    op.add_option('-f', '--force', dest='force', action='store_true',
                  help='update repository without asking for confirmation')
    (options, args) = op.parse_args(args)

    if len(args) == 0:
        sys.stderr.write('Repository name is not specified.\n')
        return 1

    # Get list of changed tree digests.
    stream = open_repository(args[0])
    all_changes = []
    for tree_digest in read_repository(stream):
        changes = tree_digest.diff()
        if len(changes) > 0:
            all_changes.append((tree_digest, changes))

    if len(all_changes) > 0:
        # Present user with the list of changes.
        print("Changed since last update:")
        for (_, changes) in all_changes:
            print_changes(changes)

        # Ask if user wants to update repository. Do not ask if --force
        # was passed as command-line argument.
        if not options.force:
            while True:
                sys.stdout.write("Do you want to update repository "
                                 "checksums? (Y/N): ")
                answer = sys.stdin.readline()[:-1].lower()
                if answer == 'y':
                    break
                elif answer == 'n':
                    return 0

        # Update tree digests and write them into repository.
        for (tree_digest, changes) in all_changes:
            tree_digest.apply_changes(changes)
        write_repository(args[0], [s for (s, _) in all_changes])
    else:
        print("Nothing changed since last time.")

    return 0

def invoke_status(args):
    """
    An entry point for the 'status' command.
    Shows repository status.
    """
    op = optparse.OptionParser(
            usage='%prog status [options] repository-name',
            description='Show repository status.')
    (options, args) = op.parse_args(args)

    if len(args) == 0:
        raise ApplicationError('Repository name is not specified.')

    stream = open_repository(args[0])
    for snapshot in read_repository(stream):
        print_changes(snapshot.diff())

    return 0

def invoke_verify(args):
    """
    An entry point for the 'verify' command.
    Checks integrity of specific repository.

    The `args` parameter contains all user-specified command-line
    arguments for this action.
    """
    op = optparse.OptionParser(
            usage='%prog verify [options] repository-name',
            description='Check repository integrity.')
    op.add_option('-q', '--quiet', dest='quiet', action='store_true',
                  help='be less verbose')
    (options, args) = op.parse_args(args)

    if len(args) == 0:
        sys.stderr.write('Repository name is not specified.\n')
        return 1

    # Load tree digests and verify live filesystem against them.
    stream = open_repository(args[0])
    damaged_files = []
    for tree_digest in read_repository(stream):
        verify_tree(tree_digest, damaged_files, quiet=options.quiet)
    stream.close()

    if len(damaged_files) == 0:
        print('Repository is in \033[92mGOOD\033[0m state.')
        return 0
    else:
        # Show overview of bad files, this should help user to faster
        # survey the damage.
        print('The following files are in a bad shape:')
        for (live_digest, stored_digest, file_state) in damaged_files:
            if file_state == FileState.DAMAGED:
                print_damaged_file_info(live_digest, stored_digest)
            else:
                print_bad_file_info(stored_digest, file_state)

        print('\nRepository is in \033[91mDAMAGED\033[0m state.')
        return 1

    return 1

def show_help():
    """
    Prints help text into stdout.
    """
    print('Usage: %s [action] [action-options]\n'
          'Use \'--help\' after an action name to get more information\n'
          'about specific action.\n'
          '\n'
          'Actions:\n'
          '  new          Create new repository.\n'
          '  apply        Apply changes to repository.\n'
          '  status       Shows repository status.\n'
          '  verify       Verify repository.\n'
          '  help         Show help text and quit.' % (sys.argv[0]))

if __name__ == '__main__':
    if len(sys.argv) < 2:
        show_help()
        exit(1)

    locale.setlocale(locale.LC_ALL, '')

    code = 0
    try:
        if sys.argv[1] == 'new':
            code = invoke_new(sys.argv[2:])
        elif sys.argv[1] == 'apply':
            code = invoke_apply(sys.argv[2:])
        elif sys.argv[1] == 'status':
            code = invoke_status(sys.argv[2:])
        elif sys.argv[1] == 'verify':
            code = invoke_verify(sys.argv[2:])
        else:
            show_help()
            code = 0
    except ApplicationError as e:
        sys.stderr.write('%s.\n' % (e))
        exit(1)
    except KeyboardInterrupt as e:
        exit(1)

    exit(code)
